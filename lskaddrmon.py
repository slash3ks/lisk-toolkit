#!/usr/bin/env python3

import os
import sys
import yaml
import smtplib
import argparse
import requests
try:
    from twilio.rest import Client
except ImportError:
    sys.exit("Please install twilio. sudo pip3 install twilio")

#TODO on first run gather transactions and make that the baseline
#TODO catch errors from twilio
#TODO catch errors if api does not respond

def get_config(location):
    """
    """
    home_path = os.path.dirname(os.path.realpath(__file__))

    try:
        with open("{}/{}".format(home_path, location)) as ymlfh:
            data = yaml.safe_load(ymlfh)
    except IOError:
        msg = 'Could not open configuration file from default path.'
        msg += ' Please copy the example file and replace required values'
        sys.exit(msg)

    return data

def api_stuff(api_url, api_uri):
    """
    """
    req_resp = requests.get(api_url + api_uri)

    return req_resp.json()

def search_sent_by_addrs(configuration, addresses):
    """
    """
    api_url = configuration['api_url']
    api_uri = configuration['api_sent_tx']

    notify = []

    # Check all addresses given by config or command line
    for address_data in addresses:

        # Split the address-amount format
        address = address_data.split('-')[0]

        try:
            tx_amount = int(address_data.split('-')[1])
        except:
            tx_amount = 0

        # Get the last 10
        api_resp = api_stuff(api_url, api_uri.format(address) +
                             '&orderBy=height:desc&limit=10')

        total_tx_count = int(api_resp['count'])

        # Get the diff
        if tx_amount == total_tx_count:
            resp_dict = {'senderId': address}
            notify.append(([resp_dict], total_tx_count))
        elif tx_amount > 0 and total_tx_count > tx_amount:
            diff = total_tx_count - tx_amount
            notify.append((api_resp['transactions'][:diff], total_tx_count))
        else:
            notify.append((api_resp['transactions'], total_tx_count))
 
    return notify

def prepare_and_send_message(configuration, response, email, sms):
    """
    """

    if response:

        messages_sent = []

        # For each response of each address
        for resp in response:

            message_text = None

            # Grab the data and the amount of the response
            resp_data, resp_amount = resp

            # if there is 1 transacion diff
            if len(resp_data) == 0 and not resp_data[0].get('id'):
                continue
            elif len(resp_data) == 1 and resp_data[0].get('id'):

                transaction_url = configuration['explorer_url_tx']
                message_text = 'Activity Detected. Check TX: {} Recipient: {}'\
                                .format(transaction_url.format(resp_data[0]['id']),
                                        resp_data[0]['recipientId'],
                                        resp_data[0]['amount'])

            elif len(resp_data) > 1 and resp_data[0].get('id'):

                message_text = 'Activity Detected. More than 1 transaction Made'

            if message_text:

                if sms:
                    resp = send_message(configuration, message_text)

                    messages_sent.append(message_text)

                    # If message fails log
                    if resp is False:
                        messages_sent.append("Failed to send message: {}"
                                              .format(message_text))
                if email:
                    resp = send_email(configuration, resp_data)

                    messages_sent.append(message_text)

                    # If message fails log
                    if resp is False:
                        messages_sent.append("Failed to send message: {}"
                                              .format(message_text))


        if len(messages_sent) >= 1:
            return True
        else:
            return False

    else:

        return False

def send_message(configuration, message):
    """
    """
    twilio = Client(configuration['account_sid_'],
                    configuration['account_auth'])
    

    message = twilio.messages.create(body=message,
                                     from_=configuration['twilio_number'],
                                     to=configuration['dst_number'])

    if message.sid:
        return True
    else:
        return False


def send_email(configuration, message):
    """
    """

    toaddr = configuration['to_email']
    fromaddr = configuration['from_email']
    username = configuration['from_email']
    subject = 'Activity Detected in one of the monitored accounts'

    msg = "\r\n".join([
        "From: {}".format(fromaddr),
        "To: {}".format(toaddr),
        "Subject: {}".format(subject),
        "",
        "Reason: {}".format(message),
        ])

    server = smtplib.SMTP('smtp.gmail.com:587')
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(username,str(configuration['app_phrase']))
    server.sendmail(fromaddr, toaddr, msg)
    server.quit()

def config_file_update(location, configuration, response):
    """
    """

    configuration['addresses'] = ["{}-{}".format(x[0][0]['senderId'], x[1])
                                  for x in response]

    home_path = os.path.dirname(os.path.realpath(__file__))

    try:
        with open("{}/{}".format(home_path, location), "w") as ymlfh:
            yaml.dump(configuration, ymlfh)
            return True
    except IOError:
        return False


def main(location, addresses, email, sms):
    """
    """
    # Gather configuration items from file
    configuration = get_config(location)

    # Read addresses from the command line or the config file
    if not addresses: addresses = configuration.get('addresses')
    
    # Search by address
    search_response = search_sent_by_addrs(configuration, addresses)

    if search_response:

        # Prepare and send the message if needed
        config_need_update = prepare_and_send_message(configuration,
                                                      search_response,
                                                      email, sms)
        if config_need_update is True:

            #Update config file with the new numbers
            resp = config_file_update(location, configuration, search_response)
            if resp is False:
                print("Failed to update config file with new data")


if __name__ == '__main__':

    PARSER = argparse.ArgumentParser(description='')

    PARSER.add_argument('-a', '--addresses', dest='addresses', action='store',
                        nargs='+', help='Delegate addresses')

    PARSER.add_argument('-c', '--config-file', dest='config_file',
                        default="lskaddrmon.main.yml", action='store',
                        help='Config file. Default is ~/lisk-toolkit/lskaddrmon.main.yml')

    PARSER.add_argument('-s', '--sms', dest='sms',
                        default="lskaddrmon.main.yml", action='store_true',
                        help='')

    PARSER.add_argument('-t', '--email', dest='email',
                        default="lskaddrmon.main.yml", action='store_true',
                        help='')


    ARGS = PARSER.parse_args()

    main(ARGS.config_file, ARGS.addresses, ARGS.email, ARGS.sms)

