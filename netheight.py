#!/usr/bin/env python

import sys
import logging
import requests
import argparse
import json


def get_config_file(lisk_dir):

    try:

        with open(lisk_dir + '/config.json') as ldfh:

            lisk_config = json.load(ldfh)

            return lisk_config

    except IOError:

        sys.exit('Could not open the config file from specified dir')

def get_network_height(config_file, port, url):
    '''
    Get the height of the network
    '''

    data = {
        'version' : '{}'.format(config_file['version']),
        'Accept': '*/*',
        'Connection': 'close',
        'os': 'linux3.13.0-85-generic',
        'port': '{}'.format(port),
        'accept': 'application/json',
        'User-Agent': 'https://github.com/blakeembrey/popsicle',
        'Accept-Encoding': 'gzip, deflate',
        'nethash': config_file['nethash']
    }

    height_list = []
    network_height = []

    peer_list = '{}:{}/peer/list'.format(url, port)

    peer_height = 'http://{}:{}/peer/height'

    try:
        resp = requests.get(peer_list, headers=data).json()
    except ValueError:
        raise
        pass

    for peer in resp['peers']:

        try:
            res = requests.get(peer_height.format(peer['ip'], port),
                               headers=data,  timeout=0.3).json()

            height_list.append({'height': res['height'], 'ip': peer['ip']})
            network_height.append(res['height'])

        except requests.exceptions.ConnectionError:
            pass
        except requests.exceptions.Timeout:
            pass
        except KeyError:
            pass
        except ValueError:
            pass

    if height_list:
        logging.info('Max network height: {}'.format(max(network_height)))

        #for height in height_list:
            #print height
            #print json.dumps(dict(height_list), indent=2)
        return max(network_height)
    else:
        return 0


def main():

    config_file = get_config_file(args.liskdir)

    max_network_height = get_network_height(config_file, args.port, args.url)

    print max_network_height


if __name__ == '__main__':

    parser = argparse.ArgumentParser(usage='soon')

    parser.add_argument('-l','--log',dest='log',action='store',
        choices={'info','debug','warning','error','critical'},
        help='verbose mode')

    parser.add_argument('-lf','--log-file',dest='logfile',action='store',
                        help='verbose mode')

    parser.add_argument('-u', '--url', dest='url', action='store',
                        default='http://localhost',
                        help='Url in format: http://localhost')

    parser.add_argument('-p', '--port', dest='port', action='store',
                        default='7000', help='Port used by the application')

    parser.add_argument('-ld','--lisk-dir',dest='liskdir',action='store',
                        default='/opt/lisk/lisk-main/', help='Lisk dir')

    args = parser.parse_args()

    logger = logging.getLogger(__name__)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.getLogger("requests").setLevel(logging.WARNING)

    LEVELS = { 'debug':logging.DEBUG,
                'info':logging.INFO,
                'warning':logging.WARNING,
                'error':logging.ERROR,
                'critical':logging.CRITICAL,
                }

    if args.log:

        if args.logfile:
            logging.basicConfig(level=LEVELS[args.log],
                                format='%(asctime)s %(funcName)s %(message)s ',
                                filename=args.logfile,
                                datefmt='%m/%d/%Y %I:%M:%S %p')

        else:
            logging.basicConfig(level=LEVELS[args.log],
                                format='%(asctime)s %(funcName)s %(message)s ',
                                datefmt='%m/%d/%Y %I:%M:%S %p')
    else:

        logging.basicConfig(level=logging.CRITICAL)

    main()
