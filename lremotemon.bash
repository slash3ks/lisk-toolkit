#!/bin/bash

# Constants

APP="$(pwd)/lremotemon"
APP_LOG=""
SVC_LOG=""
PID_FILE=""
PORT=""
DPOS=""
CONFIG=""
CONFIG_DIR=""

LSK_MAIN_CONFIG='https://raw.githubusercontent.com/LiskHQ/lisk/master/config.json'
LSK_TEST_CONFIG='https://raw.githubusercontent.com/LiskHQ/lisk/development/config.json'
OXY_MAIN_CONFIG='https://raw.githubusercontent.com/Oxycoin/oxy-node/master/config.json'
OXY_TEST_CONFIG='https://raw.githubusercontent.com/Oxycoin/oxy-node/testnet/config.json'
LWF_MAIN_CONFIG='https://raw.githubusercontent.com/lwfcoin/lwf-node/master/config.json'
LWF_TEST_CONFIG='https://raw.githubusercontent.com/lwfcoin/lwf-node/testnet/config.json'
SHIFT_MAIN_CONFIG='https://raw.githubusercontent.com/ShiftNrg/shift/master/config.json'
SHIFT_TEST_CONFIG='https://raw.githubusercontent.com/ShiftNrg/shift/testnet/config.json'
ARK_MAIN_CONFIG=''
ARK_TEST_CONFIG=''

# END Constants

start() {
    echo $(date +"%Y-%m-%d %H:%M:%S:") "lremotemon service start requested" | tee -a ${SVC_LOG}

    # 
    if [ -z "$LRMON_FILE" ]; then
        echo -n "Mem password: "
        read -s mem_pw
        echo
        export LRMON_FILE="$mem_pw"
    else
        echo "File env variable set"
    fi

    if [ -z "$LRMON_FORGING" ]; then
        echo -n "Delegate password: "
        read -s delegate_pw
        echo
        export LRMON_FORGING="$delegate_pw"
    else
        echo "File env variable set"
    fi

    if [ -z "$LRMON_NODES" ]; then
        echo -n "Nodes: "
        read delegate_nodes
        export LRMON_NODES="$delegate_nodes"
    else
        echo "File env variable set"
    fi

    username=""
    if [ -z "$LRMON_USER" ]; then
        echo -n "Username: "
        read username
        echo
    else
        username="$LRMON_USER"
    fi

    CMD_STRING="nohup $APP --log info --config $CONFIG_DIR -lg $APP_LOG --encrypt --username $username --environment --interval 30 --nodes $delegate_nodes"

    echo $(date +"%Y-%m-%d %H:%M:%S:") "$CMD_STRING" | tee -a ${SVC_LOG}

    nohup $APP --log info --config "$CONFIG_DIR" -lf "$APP_LOG" \
        --encrypt --username "$username" --environment --interval 30\
        --nodes `echo $delegate_nodes` >> ${SVC_LOG} 2>&1 &

    echo $! > $PID_FILE
    echo "$(date +"%Y-%m-%d %H:%M:%S:") lremotemon service startup complete" | tee -a ${SVC_LOG}

    # clean up env vars
    export LRMON_FILE=""
    export LRMON_FORGING=""
    export LRMON_NODES=""

}

stop() {
    echo "Stopping lremotemon service"
    echo $(date +"%Y-%m-%d %H:%M:%S:") "lremotemon service stop requested" | tee -a ${SVC_LOG}
    kill $(cat ${PID_FILE})
    RETVAL=$?
    [ $RETVAL = 0 ] && rm -f ${PID_FILE}
}

check_version() {
    echo $(date +"%Y-%m-%d %H:%M:%S:") "Checking version..."
    case "$1" in
        [Ll][Ii][Ss][Kk])
            DPOS="LISK"
            case "$2" in
                [Mm][Aa][Ii][Nn])
                    CONFIG="$LSK_MAIN_CONFIG"
                ;;
                [Tt][Ee][Ss][Tt])
                    CONFIG="$LSK_TEST_CONFIG"
                ;;
                *)
                    usage $2
                    exit 1
                ;;
            esac
        ;;
        [Ss][Hh][Ii][Ff][Tt])
            DPOS="SHIFT"
            case "$2" in
                [Mm][Aa][Ii][Nn])
                    CONFIG="$SHIFT_MAIN_CONFIG"
                ;;
                [Tt][Ee][Ss][Tt])
                    CONFIG="$SHIFT_TEST_CONFIG"
                ;;
                *)
                    usage $2
                    exit 1
                ;;
            esac
        ;;
        [Oo][Xx][Yy])
            DPOS="OXY"
            case "$2" in
                [Mm][Aa][Ii][Nn])
                    CONFIG="$OXY_MAIN_CONFIG"
                ;;
                [Tt][Ee][Ss][Tt])
                    CONFIG="$OXY_TEST_CONFIG"
                ;;
                *)
                    usage $2
                    exit 1
                ;;
            esac
        ;;
        [Aa][R][rKk])
            DPOS="ARK"
            case "$2" in
                [Mm][Aa][Ii][Nn])
                    CONFIG="$ARK_MAIN_CONFIG"
                ;;
                [Tt][Ee][Ss][Tt])
                    CONFIG="$ARK_TEST_CONFIG"
                ;;
                *)
                    usage $2
                    exit 1
                ;;
            esac
        ;;
        [Ll][Ww][Ff])
            DPOS="LWF"
            case "$2" in
                [Mm][Aa][Ii][Nn])
                    CONFIG="$LWF_MAIN_CONFIG"
                ;;
                [Tt][Ee][Ss][Tt])
                    CONFIG="$LWF_TEST_CONFIG"
                ;;
                *)
                    usage $2
                    exit 1
                ;;
            esac
        ;;
        *)
            usage $1
            exit 1
        ;;
    esac

    APP_LOG="/tmp/lremotemon-$DPOS-$2.log"
    SVC_LOG="/tmp/lremotemon-$DPOS-$2-svc.log"
    PID_FILE="/tmp/lremotemon-$DPOS-$2.pid"
    CONFIG_DIR="/tmp/$DPOS-$2-config.json"

    echo $(date +"%Y-%m-%d %H:%M:%S:") "Selection dpos: $DPOS config: $CONFIG" | tee -a ${SVC_LOG}
}

download_config() {
    echo $(date +"%Y-%m-%d %H:%M:%S:") "Downloading config from $CONFIG ..." | tee -a ${SVC_LOG}
    wget --quiet $CONFIG -O $CONFIG_DIR
    if [ ! -f ${CONFIG_DIR} ]; then
        echo $(date +"%Y-%m-%d %H:%M:%S:") "Failed to download config file" | tee -a ${SVC_LOG}
        exit 1
    else
        echo $(date +"%Y-%m-%d %H:%M:%S:") "Downloaded config file to: $CONFIG_DIR" | tee -a ${SVC_LOG}
    fi
}

usage() {
    echo $(date +"%Y-%m-%d %H:%M:%S:") "Option $1 not recognized or not entered" | tee -a ${SVC_LOG}
    echo
    echo $"Usage: $0 {start|stop|restart|status|remove} {lisk|ark|shift|oxy|lwf} {main|test}"
}

# Main
case "$1" in
    start)
        check_version $2 $3

        if [ ! -f ${PID_FILE} ]; then
            download_config $3
            start
        else
            echo  $(date +"%Y-%m-%d %H:%M:%S:") "Found PID file ${PID_FILE} . Cannot start" | tee -a ${SVC_LOG}
            exit 1
        fi
        ;;
    stop)
        check_version $2 $3

        if [ -f ${PID_FILE} ]; then
            stop
        else
            echo $(date +"%Y-%m-%d %H:%M:%S:") "Could not find PID file. Cannot stop" | tee -a ${SVC_LOG}
            exit 1
        fi
        ;;
    remove)
        check_version $2 $3
        [ -f ${PID_FILE} ] && rm -f ${PID_FILE}
        RETVAL=$?
        [ $RETVAL = 0 ] && echo $(date +"%Y-%m-%d %H:%M:%S:") "Removed ${PID_FILE}"\
            | tee -a ${SVC_LOG}
        ;;
    restart)
        stop
        start
        ;;
    status)
        check_version $2 $3
        if [ -a ${PID_FILE} ] ; then
            cat ${PID_FILE}
        else
            echo $(date +"%Y-%m-%d %H:%M:%S:") "PID file not found. lremotemon is not running for $DPOS $3"\
                | tee -a ${SVC_LOG}
        fi
        ;;
    *)
        usage $1
        exit 1
esac
exit 0

