#!/usr/bin/env python

import smtplib
import requests
import json

def lmailer(msgdict):

    m_helper = '/home/liskadmin/lisk-toolkit/mailer.json'

    try:

        with open(m_helper) as helper:

            h = json.load(helper)

            toaddr = h['to']
            fromaddr = h['from']
            username = h['from']
            subj = h['subj']

            msg = "\r\n".join([
                "From: {}".format(fromaddr),
                "To: {}".format(toaddr),
                "Subject: {}".format(subj),
                "",
                "Reason: {}".format(msgdict),
                ])

            server = smtplib.SMTP('smtp.gmail.com:587')
            server.ehlo()
            server.starttls()
            server.ehlo()
            server.login(username,str(h['phrase']))
            server.sendmail(fromaddr,toaddr,msg)
            server.quit()

    except IOError:

        pass
