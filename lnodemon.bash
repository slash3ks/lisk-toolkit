#!/bin/bash

NOW=$(date +"%Y-%m-%d %H:%M:%S:")
LOG_FILE='/tmp/lnodemon_svc.log'
PID_FILE='/tmp/lnodemon.pid'
PORT=''
liskdir=''

start() {
    echo "Starting lnodemon service on port $1 "
    echo $NOW "lnodemon service start requested" >> ${LOG_FILE}

    liskdir=""
    if [[ "$1" == "7000" ]]; then
        liskdir="test"
    else
        liskdir="main"
    fi

    /home/liskadmin/lisk-toolkit/lnodemon --log debug \
        --lisk-dir /opt/lisk/lisk-"$liskdir"/ -r remote \
        --port "$1" -lf /tmp/lnodemon.log &
    echo "lnodemon service startup"
    echo
}

starte() {
    echo "Starting lnodemon service: "
    echo $NOW "lnodemon service start requested" >> ${LOG_FILE}
    echo -n "File password: "
    read -s file_pw
    echo
    echo -n "Delegate password: "
    read -s delegate_pw
    echo
    export LNODEMON_FILE="$file_pw"
    export LNODEMON_FORGING="$delegate_pw"
    username=""
    if [ -z "$2" ]; then
        echo -n "Username: "
        read username
        echo
    else
        username="$2"
    fi
    liskdir=""
    if [[ "$1" == "7000" ]]; then
        liskdir="test"
    else
        liskdir="main"
    fi

    /home/liskadmin/lisk-toolkit/lnodemon --log debug \
        --lisk-dir /opt/lisk/lisk-"$liskdir"/ -r remote -lf /tmp/lnodemon.log \
        --port "$1" --encrypt --username "$username" --environment &
    echo "lnodemon service startup"
    echo
}

stop() {
    echo "Stopping lnodemon service"
    echo $NOW "lnodemon service stop requested" >> ${LOG_FILE}
    kill $(cat ${PID_FILE})
    RETVAL=$?
    [ $RETVAL = 0 ] && rm -f ${PID_FILE}
}

check_version() {
    version=$(grep -m1 port /opt/lisk/lisk-Linux-x86_64/config.json | \
                awk '{print $2}' | sed 's/,//g')
    PORT="$version"
}

# Main
case "$1" in
    start)
        if [ ! -f ${PID_FILE} ]; then

            check_version
            start "$PORT"
        else
            echo "Found PID file. Cannot start"
            exit 1
        fi
        ;;
    starte)
        if [ ! -f ${PID_FILE} ]; then
            check_version
            starte "$PORT"
        else
            echo "Found PID file. Cannot start"
            exit 1
        fi
        ;;
    stop)
        if [ -f ${PID_FILE} ]; then
            stop
        else
            echo "Could not find PID file. Cannot stop"
            exit 1
        fi
        ;;
    restart)
        stop
        start
        ;;
    status)
        if [ -a ${PID_FILE} ] ; then
            cat ${PID_FILE}
        else
            echo "No pid file created. lnodemon is not running"
        fi
        ;;
    *)
        echo $"Usage: $0 {start|stop|restart|status}"
        exit 1
esac
exit 0
