# lisk-toolkit
Lisk Stats and Other Stuff

```
git clone https://github.com/slasheks/lisk-toolkit.git
cd lisk-toolkit
```


lremotemon(.py) / lremotemon.bash

```

$ bash lremotemon.bash start lisk test

2017-12-07 12:18:49: Checking version...
2017-12-07 12:18:49: Selection dpos: LISK config: https://raw.githubusercontent.com/LiskHQ/lisk/development/config.json
2017-12-07 12:18:49: Downloading config from https://raw.githubusercontent.com/LiskHQ/lisk/development/config.json ...
2017-12-07 12:18:49: Downloaded config file to: /tmp/LISK-test-config.json
2017-12-07 12:18:49: lremotemon service start requested

Mem password: <random password to protect phrase in memory>

Delegate password: <forging delegate password>

Nodes: https://node1 https://node2:4443

Username: slasheks

2017-12-07 12:19:19: nohup /home/admin/lisk-toolkit/lremotemon --log info --config /tmp/LISK-test-config.json -lg /tmp/lremotemon-LISK-test.log --encrypt --username slasheks --environment --interval 30 --nodes https://node1 https://node2:4443

2017-12-07 12:19:19: lremotemon service startup complete
```
